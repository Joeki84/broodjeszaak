<?php
require_once 'data/dbconfig.php';
require_once 'data/broodjedao.php';
require_once 'data/belegdao.php';
require_once 'entities/broodje.php';
require_once 'entities/beleg.php';
require_once 'entities/gebruiker.php';
require_once 'entities/belegdbroodje.php';
require_once 'entities/bestelling.php';

class bestellingDAO {
    
    public function creerBestelling($gebruiker, $tijd){
        $dbh = new PDO(DBConfig::$DB_CONNSTRING, DBConfig::$DB_USERNAME, DBConfig::$DB_PASSWORD);
        
        $sql = "INSERT INTO bestelling (gebruikerId, datum, totaalprijs) VALUES ('" . $gebruiker->getGebruikerId() . "', '$tijd', '0')";
        
        $dbh->exec($sql);
        $bestellingId = $dbh->lastInsertId();
        
        $dbh = null;
        return $bestellingId;
    }
    
    public function creerBelegdBroodje($bestellingId, $broodje, $prijs){
        $dbh = new PDO(DBConfig::$DB_CONNSTRING, DBConfig::$DB_USERNAME, DBConfig::$DB_PASSWORD);
        
        $sql = "INSERT INTO belegdbroodje (broodjeId, bestellingId, prijs) VALUES ('" . $broodje->getBroodjeId() . "', '$bestellingId', '$prijs')";
        
        $dbh->exec($sql);
        $belegdbroodjeId = $dbh->lastInsertId();
        
        $dbh = null;
        return $belegdbroodjeId;
    }
    
    public function creerBelegPerBroodje($belegdbroodjeId,$beleg){
        $dbh = new PDO(DBConfig::$DB_CONNSTRING, DBConfig::$DB_USERNAME, DBConfig::$DB_PASSWORD);
        
        $sql = "INSERT INTO belegperbroodje (belegdbroodjeId, belegId) VALUES ('$belegdbroodjeId', '" . $beleg->getBelegId() . "')";

        $dbh->exec($sql);
        
        $dbh = null;
    }
    
    public function updateTotaalprijs($bestellingId, $totaalprijs){
        $dbh = new PDO(DBConfig::$DB_CONNSTRING, DBConfig::$DB_USERNAME, DBConfig::$DB_PASSWORD);
        
        $sql = "UPDATE bestelling SET totaalprijs = '$totaalprijs' WHERE bestellingId = '$bestellingId'";

        $dbh->exec($sql);
        
        $dbh = null;        
    }
    
    public function getLijstBestellingen($gebruiker){
        $lijst = array();
        
        $dbh = new PDO(DBConfig::$DB_CONNSTRING, DBConfig::$DB_USERNAME, DBConfig::$DB_PASSWORD);
        
        $sql = "SELECT bestellingId, gebruikerId, datum, totaalprijs FROM bestelling WHERE gebruikerId = '" . $gebruiker->getGebruikerId() . "' ORDER BY datum";
        
        $resultSet = $dbh->query($sql);
        foreach ($resultSet as $rij) {
            $bestelling = Bestelling::create($rij["bestellingId"], $rij["gebruikerId"], $rij["datum"], $rij["totaalprijs"]);
            array_push($lijst, $bestelling);
        }
        $dbh = null;
        return $lijst;
    }
    
    public function getLijstBroodjes($bestellingId){
        $lijst = array();
        
        $dbh = new PDO(DBConfig::$DB_CONNSTRING, DBConfig::$DB_USERNAME, DBConfig::$DB_PASSWORD);
        
        $sqlbelegdbroodje = "SELECT belegdbroodjeId, broodjeId, prijs FROM belegdbroodje WHERE bestellingId = '$bestellingId'";

        $resultSet = $dbh->query($sqlbelegdbroodje);
        foreach ($resultSet as $rij) {
            $broodjeId = $rij["broodjeId"];
            $broodjeserv = new broodjeService();
            $broodje = $broodjeserv->getBroodjeById($broodjeId);
            
            $sqlbeleg = "SELECT belegId FROM belegperbroodje WHERE belegdbroodjeId = '" . $rij["belegdbroodjeId"] . "'";
            $resultSetbeleg = $dbh->query($sqlbeleg);
            $lijstbeleg = array();        
            foreach ($resultSetbeleg as $rijBeleg) {
                $belegserv = new belegService();
                $beleg = $belegserv->getBelegById($rijBeleg["belegId"]);
                array_push($lijstbeleg, $beleg);
            }
            
            $prijs = $rij["prijs"];
            
            $belegdbroodje = new BelegdBroodje($broodje, $lijstbeleg, $prijs);
            array_push($lijst, $belegdbroodje);
        }
        $dbh = null;
        return $lijst;
    }
}