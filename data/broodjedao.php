<?php
require_once 'data/dbconfig.php';
require_once 'entities/broodje.php';

class broodjeDAO {
    
    public function getAlleBroodjes(){
        $broodjeslijst = array();
        
        $dbh = new PDO(DBConfig::$DB_CONNSTRING, DBConfig::$DB_USERNAME, DBConfig::$DB_PASSWORD);
        
        $sql = "SELECT broodjeId, broodje, prijs FROM broodje";
        
        $resultSet = $dbh->query($sql);
        foreach ($resultSet as $rij) {
            $broodje = Broodje::create($rij["broodjeId"], $rij["broodje"], $rij["prijs"]);
            array_push($broodjeslijst,$broodje);
        }
        
        $dbh = null;
        return $broodjeslijst;        
        
    }
    
    public function getBroodjeById($broodjeId){
        $dbh = new PDO(DBConfig::$DB_CONNSTRING, DBConfig::$DB_USERNAME, DBConfig::$DB_PASSWORD);
        
        $sql = "SELECT broodje, prijs FROM broodje WHERE broodjeId = $broodjeId";
        
        $resultSet = $dbh->query($sql);
        $rij = $resultSet->fetch();
        
        $broodje = Broodje::create($broodjeId, $rij["broodje"], $rij["prijs"]);
        
        $dbh = null;
        return $broodje;        
    }
}