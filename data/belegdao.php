<?php
require_once 'data/dbconfig.php';
require_once 'entities/beleg.php';

class belegDAO {
    
    public function getAlleBeleg(){
        $beleglijst = array();
        
        $dbh = new PDO(DBConfig::$DB_CONNSTRING, DBConfig::$DB_USERNAME, DBConfig::$DB_PASSWORD);
        
        $sql = "SELECT belegId, beleg, prijs FROM beleg";
        
        $resultSet = $dbh->query($sql);
        foreach ($resultSet as $rij) {
            $beleg = Beleg::create($rij["belegId"], $rij["beleg"], $rij["prijs"]);
            array_push($beleglijst,$beleg);
        }
        
        $dbh = null;
        return $beleglijst;
    }
    
    public function getBelegById($belegId){
        $dbh = new PDO(DBConfig::$DB_CONNSTRING, DBConfig::$DB_USERNAME, DBConfig::$DB_PASSWORD);
        
        $sql = "SELECT beleg, prijs FROM beleg WHERE belegId = $belegId";
        
        $resultSet = $dbh->query($sql);
        $rij = $resultSet->fetch();
        
        $beleg = Beleg::create($belegId, $rij["beleg"], $rij["prijs"]);
        
        $dbh = null;
        return $beleg;        
    }
}