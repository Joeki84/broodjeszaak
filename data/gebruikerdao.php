<?php
require_once 'data/dbconfig.php';
require_once 'entities/gebruiker.php';
require_once 'library/PHPMailer/class.phpmailer.php';

class gebruikerDAO{
    
    public function getGebruikerByEmail($email){
        $dbh = new PDO(DBConfig::$DB_CONNSTRING, DBConfig::$DB_USERNAME, DBConfig::$DB_PASSWORD);
        
        $sql = "SELECT gebruikerId, wachtwoord FROM gebruiker WHERE email = '$email'";

        $resultSet = $dbh->query($sql);
        $rij = $resultSet->fetch();
        
        if(count($rij) > 0){
            $gebruiker = Gebruiker::create($rij["gebruikerId"], $email, $rij["wachtwoord"]);

            $dbh = null;
            return $gebruiker;
        }else{
            $dbh = null;
            return null;
        }
    }
    
    public function getGebruikerById($gebruikerId){
        $dbh = new PDO(DBConfig::$DB_CONNSTRING, DBConfig::$DB_USERNAME, DBConfig::$DB_PASSWORD);
        
        $sql = "SELECT email, wachtwoord FROM gebruiker WHERE gebruikerId = '$gebruikerId'";

        $resultSet = $dbh->query($sql);
        $rij = $resultSet->fetch();
        
        if(count($rij) > 0){
            $gebruiker = Gebruiker::create($gebruikerId, $rij["email"], $rij["wachtwoord"]);

            $dbh = null;
            return $gebruiker;
        }else{
            $dbh = null;
            return null;
        }
    }    
    
    public function insertNieuweGebruiker($email, $wachtwoord){
        $dbh = new PDO(DBConfig::$DB_CONNSTRING, DBConfig::$DB_USERNAME, DBConfig::$DB_PASSWORD);
        
        $sql = "INSERT INTO gebruiker(email, wachtwoord) VALUES ('$email', '$wachtwoord')";
        
        $dbh->exec($sql);
        $gebruikerId = $dbh->lastInsertId();

        $gebruiker = Gebruiker::create($gebruikerId, $email, $wachtwoord);
        
        $dbh = null;
        return $gebruiker;
    }
    
    public function newWachtwoord($email, $wachtwoord){
        $dbh = new PDO(DBConfig::$DB_CONNSTRING, DBConfig::$DB_USERNAME, DBConfig::$DB_PASSWORD);
        
        $sql = "UPDATE gebruiker SET wachtwoord = '$wachtwoord' WHERE email = '$email'";
        
        $dbh->exec($sql);
        $dbh = null;        
    }
    
    public function mailNieuweGebruiker($email, $wachtwoord){
        $mail = new \PHPMailer();
        $mail->IsSMTP();
        $mail->SMTPDebug = 0;
        $mail->SMTPAuth = 'login';
        $mail->SMTPSecure = 'ssl';
        $mail->Host = 'xxxxxx';
        $mail->Port = 465;
        $mail->Username = 'xxxxxxx';
        $mail->Password = 'xxxxxxx';
        $mail->SetFrom('xxxxxx@xxx.xxx', 'Naam');
        $mail->isHTML(true);
        $mail->Subject = 'Aanmelding broodjeszaak';
        $mail->Body = 'Welkom bij de broodjeszaak,<br>'
            . 'Hieronder je login gegevens:<br>'
            . 'Emailadres: ' . $email . '<br>'
            . 'Wachtwoord: ' . $wachtwoord . '<br>'
            . 'Groetjes,<br>'
            . 'De Broodjeszaak';
        $mail->AddAddress($email);
        $mail->Send();
    }
    
    public function mailNieuwWachtwoord($email, $wachtwoord){
        $mail = new \PHPMailer();
        $mail->IsSMTP();
        $mail->SMTPDebug = 0;
        $mail->SMTPAuth = 'login';
        $mail->SMTPSecure = 'ssl';
        $mail->Host = 'xxxxxxx';
        $mail->Port = 465;
        $mail->Username = 'xxxxxxxxx@xxxxx.xxxx';
        $mail->Password = 'xxxxxxxxx';
        $mail->SetFrom('xxxxx@xxxx.xxx', 'Naam');
        $mail->isHTML(true);
        $mail->Subject = 'Nieuw wachtwoord broodjeszaak';
        $mail->Body = 'Hallo ' . $email . ',<br>'
            . 'Hieronder je nieuw wachtwoord:<br>'
            . 'Wachtwoord: ' . $wachtwoord . '<br>'
            . 'Groetjes,<br>'
            . 'De Broodjeszaak';
        $mail->AddAddress($email);
        $mail->Send();   
    }
    
}