<?php
class Bestelling{
    
    private static $idMap = array();
    
    private $bestellingId;
    private $gebruiker;
    private $datum;
    private $prijs;
    
    private function __construct($bestellingId, $gebruiker, $datum, $prijs) {
        $this->bestellingId = $bestellingId;
        $this->gebruiker = $gebruiker;
        $this->datum = $datum;
        $this->prijs = $prijs;        
    }
    
    public static function create($bestellingId, $gebruiker, $datum, $prijs){
        if(!isset(self::$idMap[$bestellingId])){
            self::$idMap[$bestellingId] = new Bestelling($bestellingId, $gebruiker, $datum, $prijs);
        }
        return self::$idMap[$bestellingId];
    }
    
    public function getBestellingId(){
        return $this->bestellingId;
    }
    
    public function getGebruiker(){
        return $this->gebruiker;
    }
    
    public function getDatum(){
        return $this->datum;
    }
    
    public function getPrijs(){
        return $this->prijs;
    } 
    
    public function setBestellingId($bestellingId){
        $this->bestellingId = $bestellingId;
    }
    
    public function setGebruiker($gebruiker){
        $this->gebruiker = $gebruiker;
    }
    
    public function setDatum($datum){
        $this->datum = $datum;
    }
    
    public function setPrijs($prijs){
        $this->prijs = $prijs;
    }
}