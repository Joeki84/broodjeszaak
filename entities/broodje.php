<?php
class Broodje{
    
    private static $idMap = array();
    
    private $broodjeId;
    private $broodje;
    private $prijs;
    
    private function __construct($broodjeId, $broodje, $prijs) {
        $this->broodjeId = $broodjeId;
        $this->broodje = $broodje;
        $this->prijs = $prijs;        
    }
    
    public static function create($broodjeId, $broodje, $prijs){
        if(!isset(self::$idMap[$broodjeId])){
            self::$idMap[$broodjeId] = new Broodje($broodjeId, $broodje, $prijs);
        }
        return self::$idMap[$broodjeId];
    }
    
    public function getBroodjeId(){
        return $this->broodjeId;
    }
    
    public function getBroodje(){
        return $this->broodje;
    }
    
    public function getPrijs(){
        return $this->prijs;
    }
    
}