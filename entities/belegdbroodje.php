<?php
class BelegdBroodje{
    
    private $broodje;
    private $beleg;
    private $prijs;
    
    public function BelegdBroodje($broodje, $beleg, $prijs){
        $this->broodje = $broodje;
        $this->beleg = $beleg;
        $this->prijs = $prijs;
    }
    
    public function getBroodje(){
        return $this->broodje;
    }
    
    public function getBeleg(){
        return $this->beleg;
    }
    
    public function getPrijs(){
        return $this->prijs;
    }
    
    public function setBroodje($broodje){
        $this->broodje = $broodje;
    }
    
    public function setBeleg($beleg){
        $this->beleg = $beleg;
    }
    
    public function setPrijs($prijs){
        $this->prijs = $prijs;
    }
    
}