<?php
class Beleg{
    
    private static $idMap = array();
    
    private $belegId;
    private $beleg;
    private $prijs;
    
    private function __construct($belegId, $beleg, $prijs) {
        $this->belegId = $belegId;
        $this->beleg = $beleg;
        $this->prijs = $prijs;        
    }
    
    public static function create($belegId, $beleg, $prijs){
        if(!isset(self::$idMap[$belegId])){
            self::$idMap[$belegId] = new Beleg($belegId, $beleg, $prijs);
        }
        return self::$idMap[$belegId];
    }
    
    public function getBelegId(){
        return $this->belegId;
    }
    
    public function getBeleg(){
        return $this->beleg;
    }
    
    public function getPrijs(){
        return $this->prijs;
    }
    
}