<?php
class Gebruiker{
    
    private static $idMap = array();
    
    private $gebruikerId;
    private $email;
    private $wachtwoord;
    
    private function __construct($gebruikerId, $email, $wachtwoord) {
        $this->gebruikerId = $gebruikerId;
        $this->email = $email;
        $this->wachtwoord = $wachtwoord;
    }
    
    public static function create($gebruikerId, $email, $wachtwoord){
        if(!isset(self::$idMap[$gebruikerId])){
            self::$idMap[$gebruikerId] = new Gebruiker($gebruikerId, $email, $wachtwoord);
        }
        return self::$idMap[$gebruikerId];
    }
    
    public function getGebruikerId(){
        return $this->gebruikerId;
    }
    
    public function getEmail(){
        return $this->email;
    }
    
    public function getWachtwoord(){
        return $this->wachtwoord;
    }
    
    public function setGebruikerId($gebruikerId){
        $this->gebruikerId = $gebruikerId;
    }
    
    public function setEmail($email){
        $this->email = $email;
    }
    
    public function setWachtwoord($wachtwoord){
        $this->wachtwoord = $wachtwoord;
    }
    
}