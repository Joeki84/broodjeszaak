<?php
require_once 'data/belegdao.php';

class belegService {
    
    public function getBeleg(){
        $belegdao = new belegDAO();
        $beleg = $belegdao->getAlleBeleg();
        return $beleg;
    }
    
    public function getBelegById($belegId){
        $belegdao = new belegDAO();
        $beleg = $belegdao->getBelegById($belegId);
        return $beleg;
    }
    
}