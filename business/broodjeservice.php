<?php
require_once 'data/broodjedao.php';

class broodjeService {
    
    public function getBroodjes(){
        $broodjedao = new broodjeDAO();
        $broodjes = $broodjedao->getAlleBroodjes();
        return $broodjes;
    }
    
    public function getBroodjeById($broodjeId){
        $broodjedao = new broodjeDAO();
        $broodje = $broodjedao->getBroodjeById($broodjeId);
        return $broodje;
    }
}