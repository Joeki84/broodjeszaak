<?php
require_once 'data/bestellingdao.php';

class bestellingService{

    public function PrijsBelegdBroodje($belegdbroodje){
        $prijs = 0;

        foreach ($belegdbroodje as $item) {
            if(is_array($item)){
                foreach ($item as $beleg) {
                    $prijs += $beleg->getPrijs();
                }
            }else{
                $prijs += $item->getPrijs();                
            }
        }
        
        return $prijs;
    }    
    
    public function BestelBroodjes($gebruiker, $tijd, $broodjes){
        $bestellingdao = new bestellingDAO();

        $bestellingId = $bestellingdao->creerBestelling($gebruiker, $tijd);
        $totaalprijs = 0;
        foreach ($broodjes as $belegdbroodje) {
            $broodje = $belegdbroodje->getBroodje();
            $prijs = $belegdbroodje->getPrijs();
            
            $belegdbroodjeId = $bestellingdao->creerBelegdBroodje($bestellingId, $broodje, $prijs);
            
            foreach($belegdbroodje->getBeleg() as $beleg){
                $bestellingdao->creerBelegPerBroodje($belegdbroodjeId, $beleg);
            }
            
            $totaalprijs += $prijs;
        }
        $bestellingdao->updateTotaalprijs($bestellingId, $totaalprijs);
    }
    
    public function getBestellingen($gebruiker){
        $lijst = array();
        $bestellingdao = new bestellingDAO();
        
        $lijstBestellingen = $bestellingdao->getLijstBestellingen($gebruiker);
        foreach ($lijstBestellingen as $bestelling) {
            $lijstBelegdebroodjes["datum"] = $bestelling->getDatum();
            $lijstBelegdebroodjes["belegdbroodje"] = $bestellingdao->getLijstBroodjes($bestelling->getBestellingId());
            array_push($lijst, $lijstBelegdebroodjes);
        }
        
        return $lijst;
    }
    
    public function vandaagBesteld($gebruiker){
        $besteld = false;
        $bestellingdao = new bestellingDAO();
        $vandaag = date("d-m-Y", time());
        
        $lijstBestellingen = $bestellingdao->getLijstBestellingen($gebruiker);
        foreach ($lijstBestellingen as $bestelling) {
            $datum = $bestelling->getDatum();
            $dag = date("d-m-Y", $datum);
            if(!strcmp($dag, $vandaag)){
                $besteld = true;
            }else{
                $besteld = false;
            }
        }
        return $besteld;        
    }
}