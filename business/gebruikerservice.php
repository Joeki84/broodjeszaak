<?php
require_once 'data/gebruikerdao.php';

class gebruikerService {
    
    private function wachtwoord() {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $wachtwoord = substr( str_shuffle( $chars ), 0, 4);
        return $wachtwoord;
    }
    
    public function NieuweGebruiker($email){
        $genwachtwoord = $this->wachtwoord();
        $wachtwoord = password_hash($genwachtwoord, PASSWORD_DEFAULT);
        
        $gebruikerdao = new gebruikerDAO();
        $gebruiker = $gebruikerdao->insertNieuweGebruiker($email, $wachtwoord);
        $gebruikerdao->mailNieuweGebruiker($email, $genwachtwoord);
        return $gebruiker;
    }
    
    public function NieuwWachtwoord($email){
        $genwachtwoord = $this->wachtwoord();
        $wachtwoord = password_hash($genwachtwoord, PASSWORD_DEFAULT);
        
        $gebruikerdao = new gebruikerDAO();
        $gebruikerdao->newWachtwoord($email, $wachtwoord);
        $gebruikerdao->mailNieuwWachtwoord($email, $genwachtwoord);
    }
    
    public function getGebruiker($email){
        $gebruikerdao = new gebruikerDAO();
        $gebruiker = $gebruikerdao->getGebruikerByEmail($email);
        return $gebruiker;
    }
    
    public function getGebruikerById($gebruikerId){
        $gebruikerdao = new gebruikerDAO();
        $gebruiker = $gebruikerdao->getGebruikerById($gebruikerId);
        return $gebruiker;
    }
}