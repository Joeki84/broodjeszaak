<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Broodjeszaak</title>
    </head>
    <body>
        <h1>
            Overzicht
        </h1>
        <h3>
            Hallo, <?php print($gebruiker->getEmail());?>            
        </h3>
        <p>
            Winkelmandje:
        </p>
        <table border="1">
            <tr>
                <th>
                    Broodje
                </th>
                <th>
                    Beleg
                </th>
                <th>
                    Prijs
                </th>
            </tr>
            <?php
            if(isset($broodjeslijst)){
                foreach ($broodjeslijst as $belegdbroodje) {
                ?>
                <tr>
                    <td>
                        <?php print($belegdbroodje->getBroodje()->getBroodje());?>
                    </td>
                    <td>
                        <?php
                        $i=0;
                        foreach ($belegdbroodje->getBeleg() as $beleg) {
                            if($i > 0){
                                print(", ");
                            }
                            print($beleg->getBeleg());
                            $i++;
                        }
                        ?>
                    </td>
                    <td>
                        <?php print($belegdbroodje->getPrijs());?>
                    </td>
                </tr>
                <?php
                }
            }
            ?>
        </table>
        <p>
            <?php
            if(isset($telaat)){
                if(isset($besteld)){
                if($besteld){
                ?>
                    Je hebt vandaag reeds een broodje(s) besteld.
                <?php
                }else{
                    if($telaat){
                    ?>
                        Te laat om vandaag nog broodje(s) te bestellen.
                    <?php
                    }else{
                    ?>
                        <a href="index.php?actie=broodje">Stel broodje samen</a> - <a href="index.php?actie=bestel">Bestel broodjes</a>
                    <?php
                    }
                }
            }else{
            ?>
                <a href="index.php?actie=broodje">Stel broodje samen</a> - <a href="index.php?actie=bestel">Bestel broodjes</a>
            <?php    
            }
            }
            ?>
        </p>
        <p>
            Reeds besteld:
        </p>
        <table border="1">
            <tr>
                <th>
                    Datum
                </th>
                <th>
                    Broodje
                </th>
                <th>
                    Beleg
                </th>
                <th>
                    Prijs
                </th>
            </tr>
            <?php
            if(isset($vorigebestellingen)){
                foreach ($vorigebestellingen as $rij) {
                    $datum = date("d-m-Y H:i:s", $rij["datum"]);
                    foreach ($rij["belegdbroodje"] as $belegdbroodje) {
                ?>
                <tr>
                    <td>
                        <?php print($datum);?>
                    </td>
                    <td>
                        <?php print($belegdbroodje->getBroodje()->getBroodje());?>
                    </td>
                    <td>
                        <?php
                        $i=0;
                        foreach ($belegdbroodje->getBeleg() as $beleg) {
                            if($i > 0){
                                print(", ");
                            }
                            print($beleg->getBeleg());
                            $i++;
                        }
                        ?>
                    </td>
                    <td>
                        <?php print($belegdbroodje->getPrijs());?>
                    </td>
                </tr>
                <?php
                    }
                }
            }
            ?>
        </table>
        <p>
            <a href="index.php?actie=loguit">Loguit</a>
        </p>
    </body>
</html>