<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Broodjeszaak</title>
    </head>
    <body>
        <h1>
            Keuze broodje
        </h1>
        <form action="index.php?actie=broodje" method="post">
            <label id="keuzebroodje">Broodje:</label>
            <select name="broodjeId">
                <?php
                foreach ($broodjeslijst as $broodje) {
                    ?>
                    <option value="<?php print($broodje->getBroodjeId());?>">
                        <?php print($broodje->getBroodje());?>
                    </option>
                <?php
                }
                ?>
            </select><br>
            <input type="submit" value="Ga naar beleg">
        </form>
        <p>
            <a href="index.php?actie=loguit">Uitloggen</a> - <a href="index.php">Overzicht</a>
        </p>
    </body>
</html>