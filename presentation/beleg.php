<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Broodjeszaak</title>
    </head>
    <body>
        <h1>
            Keuze beleg
        </h1>
        <form action="index.php?actie=beleg" method="post">
            <label id="keuzebroodje">Beleg:</label>
            <select name="belegId">
                <?php
                foreach ($beleglijst as $beleg) {
                    ?>
                    <option value="<?php print($beleg->getBelegId());?>">
                        <?php print($beleg->getBeleg());?>
                    </option>
                <?php
                }
                ?>
            </select><br>
            <input type="submit" name="submit" id="einde" value="Einde beleg">
            <input type="submit" name="submit" id="volgend" value="Nog beleg toevoegen">
        </form>
        <p>
            <a href="index.php?actie=loguit">Uitloggen</a> - <a href="index.php">Overzicht</a>
        </p>
    </body>
</html>