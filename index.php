<?php
require_once 'entities/belegdbroodje.php';
require_once 'business/belegservice.php';
require_once 'business/broodjeservice.php';
require_once 'business/gebruikerservice.php';
require_once 'business/bestellingservice.php';

session_start();

$telaat = true;
$eindtijd = mktime(9, 59, 59);
$eindebesteltijd = date("H:i:s", $eindtijd);
$tijd = time();
$uur = date("H:i:s", $tijd);
if($eindebesteltijd > $uur){
    $telaat = false;
}

if(isset($_GET["actie"])){
    $actie = $_GET["actie"];
    switch ($actie) {
        case "broodje":
            if(isset($_POST["broodjeId"])){
                $broodjeId = $_POST["broodjeId"];
                $broodjeserv = new broodjeService();
                $broodje = $broodjeserv->getBroodjeById($broodjeId);
                $belegdbroodje["broodje"] = $broodje;
                $_SESSION["belegdbroodje"] = $belegdbroodje;
                $belegserv = new belegService();
                $beleglijst = $belegserv->getBeleg();
                include_once 'presentation/beleg.php';
            }else{
                $broodjeserv = new broodjeService();
                $broodjeslijst = $broodjeserv->getBroodjes();
                include_once 'presentation/broodje.php';
            }
            break;
        case "beleg":
            if(isset($_POST["belegId"])){
                $broodjes = array();
                $belegdbroodje = $_SESSION["belegdbroodje"];
                $belegId = $_POST["belegId"];
                $belegserv = new belegService();
                $beleg = $belegserv->getBelegById($belegId);
                $belegdbroodje["beleg"][] = $beleg;
                $_SESSION["belegdbroodje"] = $belegdbroodje;
                if($_POST["submit"] == "Nog beleg toevoegen"){
                    $belegserv = new belegService();
                    $beleglijst = $belegserv->getBeleg();
                    include_once 'presentation/beleg.php';
                }elseif ($_POST["submit"] == "Einde beleg") {
                    $bestellingserv = new bestellingService();
                    $prijs = $bestellingserv->PrijsBelegdBroodje($belegdbroodje);
                    $belegdbroodje["prijs"] = $prijs;
                    $broodjeaf = new BelegdBroodje($belegdbroodje["broodje"], $belegdbroodje["beleg"], $belegdbroodje["prijs"]);
                    unset($_SESSION["belegdbroodje"]);
                    if(isset($_SESSION["broodjes"])){
                        $broodjes = $_SESSION["broodjes"];
                    }
                    array_push($broodjes, $broodjeaf);
                    $_SESSION["broodjes"] = $broodjes;
                    $broodjeslijst = $broodjes;
                    $gebruikerId = $_SESSION["gebruikerId"];
                    $gebruikerserv = new gebruikerService();
                    $gebruiker = $gebruikerserv->getGebruikerById($gebruikerId);
                    $vorigebestellingen = $bestellingserv->getBestellingen($gebruiker);
                    include_once 'presentation/overzicht.php';
                }
            }
            break;
        case "bestel":
            if(isset($_SESSION["broodjes"])){
                $bestellingserv = new bestellingService();
                $gebruikerId = $_SESSION["gebruikerId"];
                $gebruikerservice = new gebruikerService();
                $gebruiker = $gebruikerservice->getGebruikerById($gebruikerId);
                if(!$telaat){
                    $besteld = $bestellingserv->vandaagBesteld($gebruiker);
                    if(!$besteld){
                        $broodjes = $_SESSION["broodjes"];
                        $bestellingserv->BestelBroodjes($gebruiker, $tijd, $broodjes);
                    }
                }
                unset($_SESSION["broodjes"]);
                //$vorigebestellingen = $bestellingserv->getBestellingen($gebruiker);
                //include_once 'presentation/overzicht.php';
                header("location: index.php");
            }else{
                header("location: index.php");
            }
            break;
        case "login":
            if(isset($_POST["email"])){
                $email = $_POST["email"];
                $wachtwoord = $_POST["wachtwoord"];
                $gebruikerserv = new gebruikerService();
                $gebruiker = $gebruikerserv->getGebruiker($email);
                if(password_verify($wachtwoord, $gebruiker->getWachtwoord())){
                    $_SESSION["gebruikerId"] = $gebruiker->getGebruikerId();
                }
                header("location: index.php");
            }else{
                include_once 'presentation/login.php';
            }
            break;
        case "nieuwegebruiker":
            if(isset($_POST["email"])){
                $email = $_POST["email"];
                $gebruikerserv = new gebruikerService();
                $gebruiker = $gebruikerserv->NieuweGebruiker($email);
                include_once 'presentation/login.php';
            }else{
                include_once 'presentation/nieuwelogin.php';
            }
            break;
        case "nieuwwachtwoord":
            if(isset($_POST["email"])){
                $email = $_POST["email"];
                $gebruikerserv = new gebruikerService();
                $gebruiker = $gebruikerserv->NieuwWachtwoord($email);
                include_once 'presentation/login.php';
            }else{
                include_once 'presentation/nieuwwachtwoord.php';
            }
            break;
        case "loguit":
            session_destroy();
            include_once 'presentation/login.php';
            break;
        default:
            break;
    }
}else{
    if(isset($_SESSION["gebruikerId"])){
        $gebruikerId = $_SESSION["gebruikerId"];
        $gebruikerserv = new gebruikerService();
        $gebruiker = $gebruikerserv->getGebruikerById($gebruikerId);
    }
    if(isset($gebruikerId)){
        if(isset($_SESSION["broodjes"])){
            $broodjeslijst = $_SESSION["broodjes"];
        }
        $bestellingserv = new bestellingService();
        $vorigebestellingen = $bestellingserv->getBestellingen($gebruiker);
        $besteld = $bestellingserv->vandaagBesteld($gebruiker);
        include_once 'presentation/overzicht.php';
    }else{
        include_once 'presentation/login.php';
    }
}